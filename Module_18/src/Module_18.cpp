#include <iostream>

#include "Stack.hpp"

int main()
{
	Stack<int> *stack = new Stack<int>;

	for (int i = 0; i < 10; i++)
	{
		stack->Push(i + 1);
	}

	while (stack->get_length() != 0)
	{
		int *element = stack->Pop();
		std::cout << element << " " << *element << std::endl;
	}

	system("PAUSE");

	return 0;
}
