#ifndef __STACK_H__
#define __STACK_H__

template <class T>
class Stack
{
private:
	T *container = nullptr;
	int length = 0;

	void CopyContainer(T *copy_from, T *copy_to)
	{
		for (int i = 0; i < length; i++)
		{
			copy_to[i] = copy_from[i];
		}
	}

public:
	Stack() { container = new T[length]; }

	Stack(const int &length_) { container = new T[length_]; }

	~Stack() { delete[] container; }

	int get_length() { return length; }

	void Push(const T &element)
	{
		T *temp_container = new T[length + 1];
		CopyContainer(container, temp_container);
		temp_container[length] = element;

		length++;

		delete[] container;
		container = new T[length];
		CopyContainer(temp_container, container);

		delete[] temp_container;
	}

	T *Pop()
	{
		length--;
		T element = container[length];

		T *temp_container = new T[length];
		CopyContainer(container, temp_container);

		delete[] container;
		container = new T[length];
		CopyContainer(temp_container, container);

		delete[] temp_container;
		return &element;
	}
};
#endif // __STACK_H__